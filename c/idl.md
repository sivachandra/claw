# The C wrapper IDL

This document presents the Claw IDL for automatic generation of C wrapper
libraries of C++ libraries. The IDL file is essentially a C++ header file
explicitly listing the C API to be generated. Following the Claw convention,
this file should have an extension of `.c.clif`.

Claw reads the IDL listed in a `.c.clif` file and generates the corresponding
wrapper library in C. If the name of the IDL file is `wrapper.c.clif`, then the
generated library consists of a header file with name `wrapper.h`, and an
implentation file with name `wrapper.c`. One should be able to compile the
implentation file into a static or dynamic library as suitable, and link with
their C executable.

After reading in the IDL file, Claw performs a number of checks. These checks
are performed to verify that the desired C API (as listed in the IDL file) can
be generated at all. Most checks essentially boil down to verifying that the C++
constructs, with names as deduced from the wrappers listed in the IDL file,
exist in the C++ library at all. Next, if the C++ construct do exist, Claw
checks if the wrappers can be generated as specified by the IDL file. If all of
Claws checks are successful, then the wrapper for the C++ constructs will be
generated. The rest of this document presents various constructs one can use
to speficy the wrappers in the IDL file.

## Header files

One has to list the header files in which Claw should look up the C++ constructs
that are to be wrapped. This is done using the normal `#include` preprocessor
directives:

```c++
#include <path/to/my/header/file1.h>
#include <path/to/my/header/file2.h>
...
```

The header files *should* have an extension of `.h` and one can `#include` more
than one of them. It is an error to wrap C++ entities which are not directly
declared in the listed header files.

## Library prefix

The C language does not have namespaces. Hence, to avoid name collisions with
other C APIs, each C API typically uses its own distinctive prefix to name the
entities in its public interface. This prefix is specified in the IDL by
setting a global variable named `__CLAW_WRAPPER_LIBRARY_PREFIX` to the desired
prefix string value.

```c++
__CLAW WRAPPER_LIBRARY_PREFIX = <prefix>;
```

In general, the `<prefix>` string can be any valid C identifier specified as a
literal string in double quotes. Before looking up the corresponding C++ name,
Claw strips this prefix from the C name listed in the IDL.

For examples in this document, we will use a library prefix of `"LLVM"`.

```c++
__CLAW_WRAPPER_LIBRARY_PREFIX = "LLVM";
```

## Name styles

Most C++ libraries use a naming style for their public entities. To help Claw
lookup the wrapped C++ entities using the correct naming style, one has to
specify the style by setting few global variables to the appropriate value.

```c++
__CLAW_STRUCT_NAME_STYLE  = <STYLE>;
__CLAW_FUNCTION_NAME_STYLE = <STYLE>;
__CLAW_FIELD_NAME_STYLE = <STYLE>;
```

The style variables take a value from the following enumeration:

```c++
enum __CLAW_NAME_STYLE {
  __CLAW_LOWER_CAMEL_CASE,
  __CLAW_UPPER_CAMEL_CASE,
  __CLAW_SNAKE_CASE,
}
```

For examples in this document, we will use the following naming style
specifications:

```c++
__CLAW_STRUCT_NAME_STYLE = __CLAW_UPPER_CAMEL_CASE;
__CLAW_FUNCTION_NAME_STYLE = __CLAW_LOWER_CAMEL_CASE;
__CLAW_FIELD_NAME_STYLE = __CLAW_SNAKE_CASE;
```

Before looking up a C++ name, Claw does the following to the declared C name:

1. Strips the [library prefix](#library-prefix).
2. Converts the stripped name into the expected target name style for the entity
being wrapped. This conversion could be a *NOP* if the stripped name is
already in target name style. In general however, a conversion might be required
as the C style and the C++ style for the entity need not be the same.

Claw looks up the C++ name obtained as a result of the above steps.

## Wrapping constructs declared in namespaces

The C language does not have an equivalent of the C++ namespace construct.
However, in the IDL, the C API is enclosed in namespace markers reflecting the
namespaces of the wrapped C++ constructs. These markers are required only to
assist Claw in associating/matching the C wrappers with the correct C++
constructs. They are not carried over in to the generated C API in any form
(they cannot be carried over as is, of course, as C does not have namespaces.)
A typical listing of namespace markers looks as follows:

```c++
begin_namespace(llvm)  // A begin namespace marker for the namespace 'llvm'.

...  // Declarations of other wrapper constructs explained below.

end_namespace(llvm)  // An end namespace marker for the namespace 'llvm'.
```

NOTE: The namespace markers are actually dummy *builtin* macros which do not
affect the wrapper declarations semantically. They can be nested to reflect
the nested namespaces in the C++ library.

## Wrapping classes

Non-POD C++ classes and structs are always wrapped as opaque struct types in C.
It is an error to wrap them as serialized struct types. To wrap a class, say
`llvm::Foo` as an opaque C struct, one simply has to declare an opaque struct in
the IDL as follows:

```c++
begin_namespace(llvm)

struct LLVMFooClass;

end_namespace(llvm)
```

When Claw sees the above declaration in the IDL file, it does the following
in order:

1. It strips the prefix `LLVM` from the name of the struct as we have
[specified that `LLVM` is the C wrapper library prefix](#library-prefix).
2. If required, the stripped name is adjusted to match the C++ name style for
structs. As we have [specified that the C++ struct name style is upper camel
case](#name-styles), there is no adjustment required in this case as the
stripped name `FooClass` is already in the upper camel case style.
3. Claw looks up the name `FooClass` in the namespace `llvm`. If a
struct of class by name `llvm::FooClass` is found, then Claw generates
an opaque struct with name `FooClass` in the C wrapper library. Else, an error
is reported.

Wrapping methods of a C++ class is presented below.

## Wrapping POD structs

POD C++ structs can be wrapped in two different ways:

1. **Serialized** - When wrapped in this fashion, a struct object is serialized
when *crossing* the language boundary, and then deserialized to obtain the
native representation in the *destination language*. The benefit of this form of
wrapping is that the access to the fields of the POD struct can be done using
the `.` operator. However, the disadvantage is that, only copies of the actual
struct values cross the language boundaries. Hence, changes made in one language
will not be visible to the other language.

2. **Opaque struct** - When wrapped in this fashion, the C++ struct type is
available in C as an opaque struct. Consequently, one can only access the fields
of the struct objects via methods. References, instead of the objects themselves,
cross the language boundaries. Hence, changes made in one language will be
visible in the other language.

### Wrapping serialized structs types

To specify that a wrapped struct type should cross language boundaries in a
serialized fashion, one should use the claw directive `__claw_serialized__`
before the struct declaration.

```c++
begin_namespace(llvm)

__claw_serialized__
struct LLVMFooSerialized {
  int int_field;
  float float_field;
};

end_namespace(llvm)
```

When Claw sees the above declaration in the IDL file, it does the following
in order:

1. It strips the prefix `LLVM` from the name of the struct as we have
[specified that `LLVM` is the C wrapper library prefix](#library-prefix).
2. If required, the stripped name is adjusted to match the C++ name style for
structs. Since we have [specified that the C++ struct name style is upper camel
case](#name-styles), there is no adjustment required in this case as the
stripped name `FooSerialized` is already in upper camel case.
3. Next, Claw looks up the name `FooSerialized` in the namespace `llvm`.
4. If a struct or class by name `FooSerialized` is found in the namespace
`llvm`, Claw checks if this class or struct is of a POD type. Since,
we have used the `__claw_serialized__` directive, it is an error if the C++
class `llvm::FooSerialized` is not of a POD type.
5. If `llvm::FooSerialized` is a POD data type, Claw verifies that the
field names and their types, as declared in the IDL file, match those of the C++
struct. It is an error if they do not match.

If all of Claw's checks are clear, a C struct by name `LLVMFooSerialized` is
generated. This generated struct can be instantiated as a normal C struct, and
its fields can be accessed using the `.` operator.

NOTE: The Claw C IDL makes use of a number of special directives similar to
`__claw_serialized__`. Under the hood, they are all implemented as C++-11
attributes.

### Wrapping C++ POD types as opaque C structs

To wrap a C++ struct as an opaque C struct, one simply has to declare a struct,
similar to [wrapping a class](#wrapping-classes).

```c++
begin_namespace(llvm)

struct LLVMFooOpaque;

end_namespace(llvm)
```

When Claw sees the above declaration in the IDL file, it does the following
in order:

1. It strips the prefix `LLVM` from the name of the struct as we have
[specified that `LLVM` is the C wrapper library prefix](#library-prefix).
2. If required, the stripped name is adjusted to match the C++ name style for
structs. Since we have [specified that the C++ struct name style is upper camel
case](#name-styles), there is no adjustment required in this case as the stripped
name `FooOpaque` is already in upper camel case.
3. Claw looks up the name `FooOpaque` in the namespace `llvm`. If a
struct or class by name `llvm::FooOpaque` is found, then Claw generates
an opaque struct with name `FooOpaque` in the C wrapper. Else, an error is
reported.

Adding wrapper methods to access the fields of this struct is presented below.

## Wrapping functions

Wrapping a function, say a C++ function `llvm::fooFunc`, is done as
follows:

```c++
begin_namespace(llvm)

<return type> LLVMFooFunc(<args>);

end_namespace(llvm)
```

When Claw sees the above declaration in the IDL file, it does the following
in order:

1. Strips the prefix `LLVM` from the name of the function as we have
[specified that `LLVM` is the C wrapper library prefix](#library-prefix).
2. The stripped name `FooFunc` is in upper camel case, but the target
function [name style we have specified](#name-styles) is lower camel case.
Hence, Claw adjusts the name to `fooFunc`.
3. Next, Claw looks for a C++ function with the adjusted name `fooFunc` in the
namespace `llvm`. An error is reported if the function is not found.
4. If the function is found, then Claw checks whether the argument types and the
return type match.
5. If argument types and return type also match, then Claw generates a C
function with name LLVMFooFunc with appropriate argument types and return type.
Else, it reports and error.

## Argument and return type declaration

An import goal of the Claw C IDL is to ensure that the declarations in the IDL
file reflect the desired C API. Hence, the argument and return types in function
declarations should consist of types already declared C in the IDL file.
One can use qualified versions (like `const`) of these types, or pointer types
of these types, as long as the base type is an already declared C type. Claw
resolves these C types to the underlying wrapped C++ types before matching the
function declarations.

Unless the base type is a serialized struct type, or a primtive/builtin type
(like `int` for example), one cannot declare functions which take arguments by
value of such types. Likewise, one can also not declare functions which return
such types by value. One can only use pointer types, or qualified versions of
the pointer types, as function argument and return types. Consequently, there
are object ownership implications. These implications are detailed in the
[**Object ownership**](#object-ownership) section below.

## Renaming

There are times when one will want to wrap a C++ function with a different
name in their C API. This can be done using a special renaming drective. This
directive should be listed immediately before the function declaration as
follows:

```c++
begin_namespace(llvm)

__claw_cppname__(cppFunc);
<return type> LLVMRenamedCppFunc(<args>);

end_namespace(llvm)
```

The name of the directive used here is `__claw_cppname__` to wrap the C++
function `cppFunc` with a function named `LLVMRenamedCppFunc` in the C wrapper
API. As always, namespace markers only exist to provide Claw with
C++ name lookup scope. They are stripped out/ignored in the generated C API.
The argument types and return type are matched according to the normal
function matching rules. However, Claw looks up the listed C++ function name as
is. That is, in this case, Claw will look for the function `cppFunc` in the
namespace `llvm` without any stripping or other name adjustments. If found, it
will generate a C function with name `LLVMRenamedCppFunc` as a wrapper for it.

Renaming directive are most useful when wrapping overloaded C++ functions.
Since the C language does not support overloading, one has to wrap the different
overloads of a C++ function with multiple C functions differing in their name.
The renaming directive helps in doing exactly that: the overloads are all
declared with a different C name but all having the same C++ name. The C
functions get associated with the right C++ function based on the argument and
return types.

One can also use the renaming directive to wrap specializations of template
functions. The template arguments are specified as additional arguments to the
renaming directive.

## Wrapping methods

Wrappers of methods of C++ classes should be declared in the following manner:

```c++
begin_namespace(llvm)

// Wrapper for class llvm::Foo
struct LLVMFoo;

// Wrapper for llvm::Foo::doIt()
void LLVMFoo_DoIt(LLVMFoo*);

// Wrapper for llvm::Foo::doIt() const
__claw_cppname__(fooClass::doIt)
void LLVMFoo_DoItConst(const LLVMFoo*);

// Wrapper for llvm::Foo::doItWithInt(int)
void LLMVFoo_DoItWithInt(LLVMFoo*, int);

// Wrapper for llvm::Foo::doItWithInt(int) const
__claw_cppname__(fooClass::doItWithInt)
void LLMVFoo_DoItWithIntConst(const LLVMFoo*, int);

end_namespace(llvm)
```

The important point when declaring a method wrapper is the first argument: The
first argument has to be of a pointer type of an already declared type, or its
const qualified version. When claw finds such a type, it treats the function
declaration as a method wrapper and deduces the method name. If Claw is able to
deduce the method name and find a method with that name in the C++ library, it
proceeds to perform argument and return type matching. If all checks are
successful, then Claw will generate the method wrapper. If Claw is unable to
deduce the method name or find a method with the deduced name, it treats the
declaration as a [function wrapper](#wrapping-functions) and generates a
function wrapper if it can.

To help Claw deduce the method name, the wrapper function name has to be
constructed in the `<TYPE_NAME>_<METHOD_NAME>` format. Claw uses the type name
of the first argument for `<TYPE_NAME>`. This type name should be of a C type
already declared in the IDL file. The rest of the name, after the `_`, is used
to deduce the method name. The deduction rules are similar to the ones which
Claw uses when deducing names of [wrapped C++ functions](#wrapping-functions).

## Field accessors

Coming soon.

## Special methods

### Wrapping constructors

If the wrapped C++ class has an undeleted default constructor, then it is always
generated in this form:

```c++
<TYPE_NAME>* <TYPE_NAME>_new(void);
```

One can wrap non-default constructors using the renaming directive and the
method wrapper syntax.

### Wrapping destructors

There is no special syntax to wrap destructors. Special functions to free Claw
wrapped objects are always generated in this form:

```c++
<TYPE_NAME>_free(<TYPE_NAME>*);
```

These *freeing* functions call the C++ destructors as necessary/suitable.

### Wrapping copy constructors

Coming soon.

### Wrapping move constructors

Coming soon.

## Operator Overloading

Coming soon.

## Object ownership

When dealing with raw pointers, which would be the case most of the time when
dealing with Claw wrapped objects, the correspoding
[*free* function](#wrapping-destructors) should be called when the object is no
longer needed.

More details coming soon.

## Standard library types

Coming soon.
