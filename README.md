# Automatic Wrapper Generation Using Clang

## Introduction

**Claw** is a tool which can auto generate a C wrapper library for a C++
API/library. A C wrapper library of a C++ library enables one to call into the
C++ library from the C library. The benefits of a such a library are (and not
limited to):

1. Eliminates the need to reinvent wheels already available in C++.

2. Projects can implement performance critical pieces in C++, but provide
wrapper libraries in the C language. Not only would this make their systems
accessible to a larger set of developers, more importantly, it enables building
their systems on a wider range of platforms.

NOTE: Claw is a shorthand for **Cl**ang-**A**uto-**W**rapper. Though the scope
of this document is currently limited to generating C wrappers of C++
libraries, it is envisioned that the core Claw framework can be extended to auto
generate wrappers in other languages like Python, Ruby, Tcl etc.

## Explicit wrapping

One can reasonably expect that a wrapper generator would automatically generate
wrappers for all C++ entities in a given header file. While such an approach is
tempting, and would be brilliant if it just worked, it leads to certain thorny
technical and people problems:

1. The wrapper author typically is only interested in a part of the C++ API.
Hence, the author would invest in making only that part idiomatic in the wrapper
language. If everything in a header file were automatically wrapped, the parts
not considered by the author would also end up in the public API of the wrapper
language. Consequently, these parts need not be idiomatic.

2. Similar to the above problem, the wrapper author would write tests only for
the part of API they understand or are interested in. The rest of the API, even
if idiomatic, will become part of the public API but exist without tests.

3. Wrapping everything in a C++ header file is technically not feasible because
a header file can contain templates and overloaded functions. The C language
does not have any equivalents into which templates and overloaded functions can
be mapped to. So, the wrapper author has to explicitly guide the wrapper
generator to pick the right overload or the right template specialization. The
wrapper author might need to rename the different overloads.

## IDL driven wrapper generation

To avoid the above problems, Google’s [CLIF](https://github.com/google/clif)
wrapper generator for Python uses an IDL (interface description language) to
explicitly list the C++ entities that one wants to wrap in Python. This way only
the entities the author is interested in would be exposed to the public API.
Further, the IDL also has constructs to make wrapper API idiomatic in Python.
The Claw wapper generation framework takes the same approach of using an IDL to
explicitly list C++ constructs to wrap.

Since the aim is to provide constructs to generate idiomatic wrapper language
APIs, it is expected that each wrapper language will have its own wrapper IDL.
The language specific IDLs are described in the suitably named sub-directories
(`c` if the wrapper language is C, `py` if the wrapper language is Python, etc.)

## IDL file extension

The IDL file extensions will be of the form `.<LANG>.clif` where `<LANG>` is the
wrapper language file extension. For example, for C, it is `c` and for Python
it is `py`.

NOTE: The end suffix `.clif` has been chosen to imply a "**Cl**aw **I**nterface
**F**ile". It also makes adopting Google's Python CLIF wrapper into the Claw
framework easy.
